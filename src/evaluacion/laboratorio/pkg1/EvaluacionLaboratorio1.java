/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluacion.laboratorio.pkg1;

import static java.lang.Math.round;
import static java.lang.Math.random;
import static java.lang.Math.pow;
import static java.lang.Math.abs;
import static java.lang.Math.min;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import static java.lang.Math.abs;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author giovanni
 */
public class EvaluacionLaboratorio1 {

    // HTTP GET request
    private static String sendGet(String url, String request_method, String user_agent) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod(request_method);

        //add request header
        con.setRequestProperty("User-Agent", user_agent);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending " + request_method + " request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine+"\n");
        }
        in.close();

        //print result
        return response.toString();

    }

    public static String generaterandomWord(int length) {
        String alphabet = new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"); //9
        int n = alphabet.length(); //10

        String result = new String();
        Random r = new Random(); //11

        for (int i = 0; i < length; i++) //12
        {
            result = result + alphabet.charAt(r.nextInt(n)); //13
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        // String url = "http://localhost:8080/wse/hola";
        Map queries = new HashMap();
        String agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36";
        // String[] methods = {"GET", "POST"};

        for (int i = 0; i < 100; i++) {
            // int random = (int)(Math.random() * ((methods.length - 1) + 1));
            // String method = methods[ random ];
            String method = "GET";
            int random = (int)(Math.random() * ((100) + 1));
            String word = generaterandomWord(random);
            String url = "http://localhost:8080/wse/"+word;
            String result = sendGet(url, method, agent);
            System.out.println(result);
        }
        System.out.println();
    }

}
